const Eris = require('eris');
const config = require('./config');

const bot = new Eris.CommandClient(process.env.token, {
  getAllUsers: true,
}, {
  prefix: config.prefix,
  ignoreSelf: true,
  ignoreBots: true,
  defaultHelpCommand: true,
  defaultCommandOptions: {
    caseInsensitive: true,
  },
});

module.exports = bot;
